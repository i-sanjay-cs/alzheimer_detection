import os
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
from flask_cors import CORS
from pyngrok import ngrok  # Import ngrok module for creating a public URL

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

ngrok.set_auth_token(os.environ.get('NGROK_AUTH_TOKEN', None))

# Load the trained model
model = load_model('best_model.h5')

# Function to preprocess the input image
def preprocess_image(image_path):
    img = image.load_img(image_path, target_size=(128, 128))  # Resize image to match input shape
    img_array = image.img_to_array(img)  # Convert image to numpy array
    img_array = preprocess_input(img_array)  # Preprocess input according to VGG16 model
    img_array = np.expand_dims(img_array, axis=0)  # Add batch dimension
    return img_array

# Function to predict class probabilities for the input image
def predict_image(image_path):
    img_array = preprocess_image(image_path)
    predictions = model.predict(img_array)
    return predictions[0]

# Function to map predicted probabilities to class labels
def get_predicted_class(predictions):
    class_labels = ['Mild Dementia', 'Moderate Dementia', 'Non-Demented', 'Very Mild Dementia']
    predicted_class_index = np.argmax(predictions)
    predicted_class = class_labels[predicted_class_index]
    predicted_percentage = predictions[predicted_class_index] * 100
    return predicted_class, predicted_percentage

# Endpoint to handle predictions
@app.route('/alzheimers_predict', methods=['POST'])
def alzheimers_predict():
    try:
        if 'file' not in request.files:
            return jsonify({'error': 'No file part'})

        file = request.files['file']

        if file.filename == '':
            return jsonify({'error': 'No selected file'})

        filename = secure_filename(file.filename)
        filepath = os.path.join('uploads', filename)
        file.save(filepath)

        predictions = predict_image(filepath)
        predicted_class, predicted_percentage = get_predicted_class(predictions)

        return jsonify({'predicted_class': predicted_class, 'confidence': predicted_percentage})

    except Exception as e:
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    # Open a Ngrok tunnel to the Flask app
    public_url = ngrok.connect(5000)  # Port 5000 is where Flask app runs
    print(' * ngrok tunnel "http://127.0.0.1:5000" -> "{}"'.format(public_url))

    # Run the Flask app
    app.run()
